package com.maruf.chitchat.messages

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.maruf.chitchat.R
import com.maruf.chitchat.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_new_message.*
import kotlinx.android.synthetic.main.user_row_new_message.view.*

class NewMessageActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_new_message)
    supportActionBar?.title = "Select User"

    fetchUsers()
  }

  companion object {
    val USER_KEY = "USER_KEY"
  }

  private fun fetchUsers() {
    val ref = FirebaseDatabase.getInstance().getReference("/users")
    ref.addListenerForSingleValueEvent(object: ValueEventListener{
      override fun onDataChange(p0: DataSnapshot) {
        val adapter = GroupAdapter<ViewHolder>()
        p0.children.forEach{
          val user = it.getValue(User::class.java)
          user?.let {
            adapter.add(UserItem(user))
          }
        }

        adapter.setOnItemClickListener { item, view ->
          val userItem = item as UserItem
          val myIntent = Intent(view.context, ChatLogActivity::class.java)
          myIntent.putExtra(USER_KEY,userItem.user)
          startActivity(myIntent)
          finish()
        }
        recyclerView.adapter = adapter
      }
      override fun onCancelled(p0: DatabaseError) {

      }
    })
  }
}

class UserItem(val user: User): Item<ViewHolder>() {
  override fun bind(viewHolder: ViewHolder, position: Int) {
    viewHolder.itemView.textUsername.text = user.username

    Picasso.get().load(user.profileUrl).into(viewHolder.itemView.userImage)
  }
  override fun getLayout(): Int {
    return R.layout.user_row_new_message
  }
}
