package com.maruf.chitchat.messages

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.maruf.chitchat.R
import com.maruf.chitchat.models.ChatMessage
import com.maruf.chitchat.models.User
import com.maruf.chitchat.showToast
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.chat_from_row.view.*
import kotlinx.android.synthetic.main.chat_to_row.view.*

class ChatLogActivity : AppCompatActivity() {
  val adapter = GroupAdapter<ViewHolder>()

  var toUser: User? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_chat_log)
    recyclerViewChatLog.adapter = adapter
    toUser = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
    supportActionBar?.title = toUser?.username

    listenForMessages()

    btnSend.setOnClickListener {
      performSendMessage()
    }
  }

  private fun listenForMessages() {
    val fromId = FirebaseAuth.getInstance().uid
    val toId = toUser?.uid
    val ref = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId")
    ref.addChildEventListener(object: ChildEventListener {
      override fun onChildAdded(p0: DataSnapshot, p1: String?) {
        val chatMessage = p0.getValue(ChatMessage::class.java)
        if(chatMessage != null) {
          if (chatMessage.fromId == FirebaseAuth.getInstance().uid) {
            val currentUser = LatestMessagesActivity.currentUser ?: return
            adapter.add(ChatFromItem(chatMessage.text, currentUser))
          } else {
            adapter.add(ChatToItem(chatMessage.text, toUser!!))
          }
        }

        recyclerViewChatLog.scrollToPosition(adapter.itemCount - 1)
      }

      override fun onCancelled(p0: DatabaseError) {

      }

      override fun onChildChanged(p0: DataSnapshot, p1: String?) {

      }

      override fun onChildMoved(p0: DataSnapshot, p1: String?) {

      }

      override fun onChildRemoved(p0: DataSnapshot) {

      }

    })
  }

  private fun performSendMessage() {
    val text = etChatLog.text.toString()
    val fromId = FirebaseAuth.getInstance().uid
    val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
    val toId = user.uid

    if(fromId == null) return
    val referance = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId").push()
    val toReferance = FirebaseDatabase.getInstance().getReference("/user-messages/$toId/$fromId").push()

    val chatMessage = ChatMessage(referance.key!!, text, fromId, toId, System.currentTimeMillis()/1000)

    referance.setValue(chatMessage).addOnSuccessListener {
      etChatLog.text.clear()
      recyclerViewChatLog.scrollToPosition(adapter.itemCount - 1)
    }

    toReferance.setValue(chatMessage)

    val latestMessageRef = FirebaseDatabase.getInstance().getReference("/latest-messages/$fromId/$toId")
    latestMessageRef.setValue(chatMessage)

    val latestMessageToRef = FirebaseDatabase.getInstance().getReference("/latest-messages/$toId/$fromId")
    latestMessageToRef.setValue(chatMessage)
  }
}

class ChatFromItem(val text: String, val user: User): Item<ViewHolder>() {
  override fun bind(viewHolder: ViewHolder, position: Int) {
    viewHolder.itemView.textView_from_row.text = text
    val uri = user.profileUrl
    val targetImageview = viewHolder.itemView.imageView_chat_from_row
    Picasso.get().load(uri).into(targetImageview)
  }
  override fun getLayout(): Int {
    return R.layout.chat_from_row
  }
}

class ChatToItem(val text: String, val user: User): Item<ViewHolder>() {
  override fun bind(viewHolder: ViewHolder, position: Int) {
    viewHolder.itemView.textView_to_row.text = text
    val uri = user.profileUrl
    val targetImageview = viewHolder.itemView.imageView_chat_to_row
    Picasso.get().load(uri).into(targetImageview)
  }
  override fun getLayout(): Int {
    return R.layout.chat_to_row
  }
}
