package com.maruf.chitchat.messages

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.maruf.chitchat.R
import com.maruf.chitchat.models.ChatMessage
import com.maruf.chitchat.models.User
import com.maruf.chitchat.registerlogin.RegisterActivity
import com.maruf.chitchat.views.LatestMessageRow
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_latest_messages.*
import kotlinx.android.synthetic.main.latest_message_row.view.*

class LatestMessagesActivity : AppCompatActivity() {
  companion object {
    var currentUser: User? = null
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_latest_messages)
    recyclerViewLatestMessages.adapter = adapter
    recyclerViewLatestMessages.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

    adapter.setOnItemClickListener { item, view ->
      val myIntent = Intent(this, ChatLogActivity::class.java)

      val row = item as LatestMessageRow
      myIntent.putExtra(NewMessageActivity.USER_KEY, row.chatPartnerUser)
      startActivity(myIntent)
    }

    listenForLatestMessages()
    fetchCurrentUser()
    verifyUserLoggedIn()
  }

  val latestMessagesMap = HashMap<String, ChatMessage>()

  private fun refreshRecyclerViewMessages() {
    adapter.clear()
    latestMessagesMap.values.forEach {
      adapter.add(LatestMessageRow(it))
    }
  }

  private fun listenForLatestMessages() {
    val fromId = FirebaseAuth.getInstance().uid
    val ref = FirebaseDatabase.getInstance().getReference("/latest-messages/$fromId")
    ref.addChildEventListener(object: ChildEventListener {
      override fun onChildAdded(p0: DataSnapshot, p1: String?) {
        val chatMessage = p0.getValue(ChatMessage::class.java) ?: return
        latestMessagesMap[p0.key!!] = chatMessage
        refreshRecyclerViewMessages()
      }
      override fun onChildChanged(p0: DataSnapshot, p1: String?) {
        val chatMessage = p0.getValue(ChatMessage::class.java) ?: return
        latestMessagesMap[p0.key!!] = chatMessage
        refreshRecyclerViewMessages()
      }
      override fun onChildRemoved(p0: DataSnapshot) {

      }
      override fun onChildMoved(p0: DataSnapshot, p1: String?) {

      }
      override fun onCancelled(p0: DatabaseError) {

      }
    })
  }

  val adapter = GroupAdapter<ViewHolder>()

  private fun fetchCurrentUser() {
    val uid = FirebaseAuth.getInstance().uid
    val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
    ref.addListenerForSingleValueEvent(object: ValueEventListener {
      override fun onDataChange(p0: DataSnapshot) {
        currentUser = p0.getValue(User::class.java)
      }
      override fun onCancelled(p0: DatabaseError) {

      }
    })
  }

  private fun verifyUserLoggedIn() {
    val uid = FirebaseAuth.getInstance().uid
    if (uid == null) {
      val myIntent = Intent(this, RegisterActivity::class.java)
      myIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
      startActivity(myIntent)
    }
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      R.id.btnSignOut -> {
        FirebaseAuth.getInstance().signOut()
        val myIntent = Intent(this, RegisterActivity::class.java)
        myIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(myIntent)
      }
      R.id.btnNewMsg -> {
        val myIntent = Intent(this, NewMessageActivity::class.java)
        startActivity(myIntent)
      }
    }
    return super.onOptionsItemSelected(item)
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.nav_menu, menu)
    return super.onCreateOptionsMenu(menu)
  }
}
