package com.maruf.chitchat.registerlogin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Patterns
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.maruf.chitchat.R
import com.maruf.chitchat.messages.LatestMessagesActivity
import com.maruf.chitchat.models.User
import com.maruf.chitchat.showToast
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
  private lateinit var auth: FirebaseAuth

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_register)

    auth = FirebaseAuth.getInstance()

    btnSignup.setOnClickListener {
      signUpUser()
    }

    btnSignin.setOnClickListener {
      startActivity(Intent(this, LoginActivity::class.java))
      finish()
    }

    btnSelectImage.setOnClickListener {
      val myIntent = Intent(Intent.ACTION_PICK)
      myIntent.type = "image/*"
      startActivityForResult(myIntent, 0)
    }
  }

  var photoUri : Uri? = null
  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)

    if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
      photoUri = data.data
      val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, photoUri)
      selectImageImageview.setImageBitmap(bitmap)
      btnSelectImage.alpha = 0f
    }
  }

  private fun signUpUser() {
    if(etUsername.text.toString().isEmpty()) {
      etUsername.error = "Please enter Username"
      etUsername.requestFocus()
      return
    }
    if(etEmail.text.toString().isEmpty()) {
      etEmail.error = "Please enter Email"
      etEmail.requestFocus()
      return
    }
    if(!Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()) {
      etEmail.error = "Please enter valid email"
      etEmail.requestFocus()
      return
    }
    if(etPassword.text.toString().isEmpty()) {
      etPassword.error = "Please enter password"
      etPassword.requestFocus()
      return
    }
    auth.createUserWithEmailAndPassword(etEmail.text.toString(), etPassword.text.toString())
      .addOnCompleteListener(this) { task ->
        if (task.isSuccessful) {
          uploadImageToFirebase()
        } else {
          showToast("Account creation failed,plz try again")
        }
      }
  }

  private fun uploadImageToFirebase() {
    if(photoUri == null) return

    val filename = UUID.randomUUID().toString()
    val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
    ref.putFile(photoUri!!).addOnSuccessListener {
      ref.downloadUrl.addOnSuccessListener {
        saveUserToFirebase(it.toString())
      }
    }
  }

  private fun saveUserToFirebase(profileUrl: String) {
    val uid = FirebaseAuth.getInstance().uid ?: ""
    val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
    val user = User(uid, etUsername.text.toString(), profileUrl)
    ref.setValue(user).addOnSuccessListener {
      showToast("Information added to Database")

      val myIntent = Intent(this, LatestMessagesActivity::class.java)
      myIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
      startActivity(myIntent)
    }
      .addOnFailureListener { 
        showToast("failed to add user" + it.toString())
    }
  }
}
