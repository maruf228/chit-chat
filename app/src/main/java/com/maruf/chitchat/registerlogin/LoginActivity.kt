package com.maruf.chitchat.registerlogin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.maruf.chitchat.R
import com.maruf.chitchat.messages.LatestMessagesActivity
import com.maruf.chitchat.showToast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btnSignin
import kotlinx.android.synthetic.main.activity_login.btnSignup
import kotlinx.android.synthetic.main.activity_login.etPassword

class LoginActivity : AppCompatActivity() {
  private lateinit var auth: FirebaseAuth

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)

    auth = FirebaseAuth.getInstance()

    btnSignin.setOnClickListener {
      doLogin()
    }

    btnSignup.setOnClickListener {
      startActivity(Intent(this, RegisterActivity::class.java))
      finish()
    }
  }

  private fun doLogin() {
    if(etEmail.text.toString().isEmpty()) {
      etEmail.error = "Please enter Email"
      etEmail.requestFocus()
      return
    }
    if(!Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()) {
      etEmail.error = "Please enter valid email"
      etEmail.requestFocus()
      return
    }
    if(etPassword.text.toString().isEmpty()) {
      etPassword.error = "Please enter password"
      etPassword.requestFocus()
      return
    }
    auth.signInWithEmailAndPassword(etEmail.text.toString(), etPassword.text.toString())
      .addOnCompleteListener(this) { task ->
        if (task.isSuccessful) {
          // Sign in success, update UI with the signed-in user's information
          val user = auth.currentUser
          updateUI(user)
        } else {
          // If sign in fails, display a message to the user.
          showToast("Login failed.")
          updateUI(null)
        }

        // ...
      }
  }

  private fun updateUI(currentUser: FirebaseUser?) {
    if(currentUser != null) {
      val myIntent = Intent(this, LatestMessagesActivity::class.java)
      myIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
      startActivity(myIntent)
      showToast("Login successful.")
    } else {
      showToast("Login failed.")
    }
  }
}